## Hi there!

Originally from [Shandong, China](https://en.wikipedia.org/wiki/Shandong), I am currently a PhD candidate in the [COL Lab](https://www.nottingham.ac.uk/research/groups/col/), [University of Nottingham, UK](https://www.nottingham.ac.uk/), under the supervision of Dr. [Rong Qu](http://www.cs.nott.ac.uk/~pszrq/index.html) and Prof. [Dario Landa-Silva](http://www.cs.nott.ac.uk/~pszjds/).

My research mainly focuses on automating the design of search algorithms supported by machine learning techniques, taking the Vehicle Routing Problems as the application.

Alongside this, I enjoy producing videos and musical pieces. I have always been passionate about music, both performance and composition. Check out my channel [HERE](https://space.bilibili.com/672490912).