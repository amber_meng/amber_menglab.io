---
title: Contact Me
comments: false
---
I'm happy to communicate about research or any of my interests. Please feel free to get in touch with any opportunities you may have.

<div align="center">COL, School of Computer Science</div>
<div align="center">University of Nottingham</div>
<div align="center">Jubilee Campus</div>
<div align="center">Wollaton Road</div>
<div align="center">Nottingham NG8 1BB</div>
<div align="center">United Kingdom</div>
<div align="center">Room: C87</div>
<div align="center">E-mail: weiyao.meng@nottingham.ac.uk</div>
