---
title: Research Summary
subtitle: Automated Design of Search Algorithms for Transportation
comments: false
---

My research is mainly about automating the design of search algorithms for solving the Vehicle Routing Problem(VRP), supported by machine learning techniques. The objective is to relief the heavy demand of effective algorithm designs for solving complex optimisation problems and explore a wider range of algorithm designs that may not reach by manual designs. 


## Research Interests

#### Real world applications:
* Vehicle Routing

#### Algorithms / techniques:
* Hyper-heuristics; Meta-heuristics
* Machine learning; Data mining

## Education
Bachelor in Computer Science and Technology from [Shandong Normal University(China)](http://www.sdnu.edu.cn), 2012-2016. My Bachelor thesis is about *The Methods for the Community Detection of Complex Network*, supervised by Prof. [Hong Wang](http://www.ischool.sdnu.edu.cn/info/1106/1020.htm).

Master in Advanced Software Engineering from [King's College London](https://www.kcl.ac.uk/index.aspx), 2016-2017. My master thesis is *How Good are Satisfycing Planners at Optimal Planning*, supervised by Dr. [Amanda Coles](https://nms.kcl.ac.uk/amanda.coles/).