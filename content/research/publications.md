# Publications

## Journal Papers
**Weiyao Meng**, Rong Qu  "Automated Design of Search Algorithms: Learning on algorithmic components", Expert Systems With Applications, [PDF](https://www.researchgate.net/publication/353397761_Automated_design_of_search_algorithms_Learning_on_algorithmic_components), [DOI: 10.1016/j.eswa.2021.115493](https://www.sciencedirect.com/science/article/pii/S0957417421009039?via%3Dihub)

Li Chen, **Weiyao Meng**, and Hong Liu "Rectification Algorithm from Single Image Based on the Main Vanishing Points." JOURNAL OF INFORMATION AND COMPUTATIONAL SCIENCE 12.7: 2871-2880. [PDF](https://www.researchgate.net/publication/276513995_Rectification_Algorithm_from_Single_Image_Based_on_the_Main_Vanishing_Points)